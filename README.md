# chat-bot

This repository contains the base logic to listen the chat box of google meet and react to out commands.

### Usage

Copy the code from the folder `/src/bot.js` and paste it in the console of your browser.

To initialize the bot you **will need** the chat box of google meet open, otherwise the bot will not be able to get new incoming messages.

```javascript
class Bot {
    ...
}

/**
 * Create the bot.
 * To make it work you should pass the classname of the chat container,
 * by default it is 'z38b6' but this might change in the future.
 * 
 * In case of error you will get a message.
 */
let bot = new Bot({ chatContainerClass: 'z38b6' });

/**
 * Initialize the bot
 */
bot.init();

/**
 * In case of error you can restart it
 */
bot.restart()

/**
 * Stops the bot
 */
bot.stop()
```