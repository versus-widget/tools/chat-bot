class Bot {
    constructor({ chatContainerClass }) {
        this.chatContainerClass = chatContainerClass || '';
        this.endpoint = 'http://localhost:3000/vote';

        this.chatContainer;
        this.chatContainerObserver;

        this.lastUser;
        this.lastMessage;
        this.lastThread;
        this.lastThreadMessageContainer;
        this.lastThreadMessageContainerdObserver;

        this.observerConfig = { attributes: true, childList: true, characterData: true };
    }

    getChatContainer() {
        const container = document.getElementsByClassName(this.chatContainerClass)[0] || [];
        if (!container) return console.log('No chat container found');
        this.chatContainer = container;
    }

    updateData() {
        const container = document.getElementsByClassName(this.chatContainerClass)[0] || [];
        this.lastThread = container.lastChild;
        this.lastUser = this.lastThread.getAttribute('data-sender-name');
        this.lastThreadMessageContainer = this.lastThread.lastChild;
        this.lastMessage = this.lastThreadMessageContainer.lastChild.innerText;
    }

    verifyCommand({ lastMessage }) {
        const normalizedMessage = lastMessage.toLowerCase() || '';
        const validCommands = [
            '/go class',
            '/go functional'
        ];

        return validCommands.includes(normalizedMessage)
            ? normalizedMessage
            : null;
    }

    async postVote({ lastUser, lastMessage }) {
        const command = this.verifyCommand(this);
        if (!command) return
        try {
            await fetch(this.endpoint, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                mode: 'no-cors',
                body: JSON.stringify({ user: lastUser, command })
            }).catch((err) => {
                console.log(err)
            })
        } catch (error) {
            console.log(error)
        }
    }

    createNewThread() {
        const self = this;
        this.lastThreadMessageContainerObserver && this.lastThreadMessageContainerObserver.disconnect();
        this.lastThreadMessageContainerObserver = null;
        this.updateData();
        this.postVote(this);

        this.lastThreadMessageContainerObserver = new MutationObserver((mutations) => {
            mutations.forEach(({ addedNodes }) => {
                if(!addedNodes) return console.log('no added nodes')
                self.lastMessage = addedNodes[0].innerText
                this.postVote(this);
            })
        });

        this.lastThreadMessageContainerObserver.observe(this.lastThreadMessageContainer, this.observerConfig);
    }

    observeChatContainer() {
        const self = this;
        this.chatContainerObserver = new MutationObserver((mutations) => {
            mutations.forEach(({ addedNodes }) => {
                const receivedNode = addedNodes[0] || undefined;
                if (!receivedNode) return console.log('no new node');
                self.createNewThread();
            });
        });

        this.chatContainerObserver.observe(this.chatContainer, this.observerConfig);
    }

    init() {
        try {
            this.getChatContainer();
            this.observeChatContainer();
        } catch (error) {
            console.log('Error starting bot, probably chat window is closed');
        }
    }

    stop() {
        this.chatContainerClass = null;
        this.endpoint = null;
        this.chatContainer = null;
        this.lastUser = null;
        this.lastThread = null;
        this.lastMessage = null;

        this.chatContainerObserver && this.chatContainerObserver.disconnect();
        this.chatContainerObserver = null;
        this.lastThreadMessageContainerObserver && this.lastThreadMessageContainerObserver.disconnect();
        this.lastThreadMessageContainerObserver = null;
    }

    restart() {
        this.stop();
        this.init();
    }
}

let bot = new Bot({ chatContainerClass: 'z38b6' });
bot.init();